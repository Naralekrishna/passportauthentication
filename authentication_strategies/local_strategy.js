const passport = require(`passport`);
const LocalStrategy = require(`passport-local`).Strategy;
const pgpool = require(`../lib/pgpool`);

passport.use(new LocalStrategy(
    async (username, password, done) => {
        try {
            const user = await pgpool.query(`SELECT id, first_name, last_name, email, password FROM public.users where lower(email) = lower($1)`, [username]);
            if (user.rowCount <= 0) { return done({ status: 400, message: `incorrect username and password` }); }
            else if ((password === user.rows[0].password) === false) { return done({ status: 400, message: `incorrect password` }); }
            else { delete user.rows[0].password; done(null, user.rows[0]); };
        } catch (error) {
            throw { status: 400, message: error.message };
        }
    }
));