const pgpool = require(`../lib/pgpool`);
const jwt = require(`jsonwebtoken`);

const users = async ({ page_number, page_count, sort_by, sort_on }) => {
    try {
        const users = await pgpool.query(`select id, first_name, last_name, email from public.users order by ${sort_on} ${sort_by} limit $1 offset $2`, [+page_count, (+page_number * +page_count)]);
        return { status: 200, message: (users.rowCount > 0) ? `data found` : `no records found`, data: users.rows };
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

const generateJwt = async ({ id, first_name, last_name, email }) => {
    try {
        const token = await jwt.sign({ id, first_name, last_name, email }, `PASSPORTAUTHENTICATION`);
        return token;
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

module.exports = { users, generateJwt };