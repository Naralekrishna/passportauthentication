var express = require('express');
var router = express.Router();
const passport = require(`passport`);

const usersService = require(`../../services/usersService`);

router.get('/', async (req, res, next) => {
  try {
    const users = await usersService.users(req.query);
    res.status(users.status).json(users);
  } catch (error) {
    res.status(error.status).json({ status: error.status, message: error.message });
  }
});

router.post('/signin', async (req, res, next) => {
  try {
    passport.authenticate(`local`, async (error, user) => {
      try {
        if (error) throw { status: error.status, message: error.message };
        if (user) { const token = await usersService.generateJwt(user); res.status(200).json({ message: `signin successful`, user, token }); }
      } catch (error) {
        res.status(error.status).json({ status: error.status, message: error.message });
      }
    })(req, res, next);
  } catch (error) {
    res.status(error.status).json({ status: error.status, message: error.message });
  }
});

module.exports = router;
