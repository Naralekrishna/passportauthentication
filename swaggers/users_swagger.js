/**
 * @swagger
 * /v1/users?page_number={page_number}&page_count={page_count}&sort_on={sort_on}&sort_by={sort_by}:
 *  get:
 *      tags:
 *        - USERS
 *      summary: Resturns users
 *      description: List of users
 *      produces:
 *          application/json
 *      parameters:
 *       - name: page_number
 *         in: path
 *         required: true
 *         type: integer
 *         example: 0
 *       - name: page_count
 *         in: path
 *         required: true
 *         type: integer
 *         example: 10
 *       - name: sort_on
 *         in: path
 *         required: true
 *         type: string
 *         example: id
 *       - name: sort_by
 *         in: path
 *         required: true
 *         type: string
 *         example: desc
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          400:
 *              description: Bad Request.
 *          500:
 *              description: Failed.
 */

/**
 * @swagger
 * definitions:
 *  Authentication:
 *      type: object
 *      required:
 *          - title
 *      properties:
 *          username:
 *              type: string
 *              required: true
 *              example: 'john@email.com'
 *          password:
 *              type: string
 *              required: true
 *              example: 'John@123'
 */

/**
 * @swagger
 * /v1/users/signin:
 *  post:
 *      tags:
 *          - AUTHENTICATION
 *      summary: Sign In
 *      description: Sign In
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: body
 *            name: request
 *            required: true
 *            example: Request body
 *            schema:
 *              $ref: '#/definitions/Authentication'
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          400:
 *              description: Bad Request.
 *          500:
 *              description: Failed.
 */